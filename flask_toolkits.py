import os
import json
import psycopg2
from config import get_db_config
# from http://flask.pocoo.org/ tutorial
from flask import (Flask, Response)
from flask.ext.api import status

app = Flask(__name__)

# folder structure = /docroot/toolkits/toolkit_versions
docroot = '/sites/static/cdn/www'


@app.route("/")
def hello():
    return "<p>Valid requests: /get-toolkits, /get-toolkit-versions/toolkit</p>"


### Toolkit utilites
def get_directories(path=''):
    try:
        directories = [ directory for directory in os.listdir(path) if os.path.isdir(os.path.join(path, directory)) ]
    except OSError:
        print "Directory not found: {0}".format(path)
        exit(1)
    directories.sort()
    return directories


@app.route("/get-toolkits")
def get_toolkits():
    toolkits = get_directories(docroot)
    return Response(json.dumps(toolkits), mimetype='application/json')


@app.route("/get-toolkit-versions/<toolkit>")
def get_toolkit_versions(toolkit):
    toolkit_path = os.path.join(docroot, toolkit)
    toolkit_versions = get_directories(toolkit_path)
    return Response(json.dumps(toolkit_versions), mimetype='application/json')
### End Toolkits


### Postgres database utilites
@app.route("/get-databases/<db_server>")
def get_databases(db_server):
    sql = """SELECT datname FROM pg_database
                WHERE datname != 'postgres' AND
                datname != 'pg_info' AND
                datname not like 'template%';"""

    data = pg_query(db_server, sql)
    
    return Response(data['json_data'], mimetype='application/json', status_code=data['code'])


@app.route("/get-roles/<db_server>")
def get_roles(db_server):
    sql = """SELECT rolname FROM pg_roles
                WHERE rolname != 'postgres' AND
                rolname != 'pg_info';"""

    data = pg_query(db_server, sql)
    
    return Response(data['json_data'], mimetype='application/json', status_code=data['code'])


## Database queries
def pg_query(server='', sql=''):
    # Get datase settings for the server
    if get_db_config(db_server):
        db = get_db_config(db_server)
    else:
        return {'code':500, 'message':'503 Service Unavailable'}

    # Create a connection
    try:
        conn = psycopg2.connect(dbname=db['name'], user=db['user'], host=db['ip'], port=db['port'], password=db['password'])
    except Exception:
        return {'code':503, 'message':"Unable to connect to the database db['name'] on db['host']"} 

    cur = conn.cursor()

    try:
        cur.execute(sql)
    except Exception:
        return {'code':503, 'message':"SQL query failed"}

    result = cur.fetchall()

    pg_data = []
    for datname in result:
        pg_data.append(datname[0].lower())

    pg_data.sort()
    json_data = json.dumps(pg_data)

    return {'code':200, 'message':json_data}


### Start up settings
if __name__ == "__main__":
    app.run()