#!/usr/bin/python

import os
import json

# path of toolkit folders
docroot = "/sites/static/cdn/www"

# Get the toolkit directories under docroot
# folder structure = /docroot/toolkit/tk_version/
try:
    toolkits = [ tk_dir for tk_dir in os.listdir(docroot) if os.path.isdir(os.path.join(docroot, tk_dir)) ]
except OSError:
    print "Directory not found: {0}".format(docroot)
    exit(1)

def get_toolkit_versions(path=''):
    try:
        tk_versions = os.listdir(os.path.join(path))
    except OSError:
        print "Directory not found: {0}".format(path)
        exit(1)
    return tk_versions

# Get the path of a toolkit
try:
    tk_path = os.path.join(docroot, toolkits[0])
except IndexError:
    print "Error: no toolkits found"
    exit(1)

print json.dumps(toolkits)
print tk_path
print json.dumps(get_toolkit_versions(tk_path))